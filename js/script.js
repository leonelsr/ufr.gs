/* Author: Leonel Rocha (leonel dot rocha at ufrgs dot br)

*/

    $(document).ready(function(e) {
        $('#ano').change(function(e) {
	      $("#cursos").html('<option>Carregando...</option>');
	      $.getJSON('/cursos.json', {ano: $('#ano').val()}, function (data) {
	        $("#cursos").empty();
	        $('<option></option>').val('todos').html('Todos').appendTo('#cursos');
	        $.each(data, function (i, v) {
	          $('<option></option>').val(i).html(v).appendTo('#cursos');
	        });
	      });
        });
        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=317316128285960";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        window.___gcfg = {lang: 'pt-BR'};
        (function() {
        var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
        po.src = 'https://apis.google.com/js/plusone.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
        })();
    });




















