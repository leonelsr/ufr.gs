<?php

$request = explode("/", $_REQUEST['a']);

//require('lsdk.php');
mb_internal_encoding("UTF-8");

if (!isset($request[1]))
{
  header("HTTP/1.1 301 Moved Permanently");
  header("Location: /listao");
  exit();
}
elseif ($request[1] == "cursos.json")
{
  $cursos = unserialize(file_get_contents("data/{$_GET['ano']}.cursos"));
  header('Cache-Control: no-cache, must-revalidate');
  header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  header('Content-type: application/json');

  sort($cursos);
  print(json_encode($cursos));
  exit();
}

$is_get = false;

// Ano
if (isset($request[2]) && is_numeric($request[2])) {
  $ano = $request[2];
}
elseif (isset($_GET['ano'])) $is_get = true;
else $ano = 2013;

// Curso
if (isset($request[3])) {
  $curso = $request[3];
}
elseif (isset($_GET['curso'])) $is_get = true;
else $curso = null;

// Semestre
if (isset($request[4]) && is_numeric($request[4])) {
  $semestre = $request[4];
}
elseif (isset($_GET['semestre'])) $is_get = true;
else $semestre = 0;

if ($is_get) {
  if ($_GET['semestre'] == 0)
    header("Location: /listao/{$_GET['ano']}/{$_GET['curso']}");
  else
    header("Location: /listao/{$_GET['ano']}/{$_GET['curso']}/{$_GET['semestre']}");
  exit();
}


$arquivos = glob('data/*.data');
$anos = array();

foreach ($arquivos as $a) {
  $anos[] = end(explode('/', reset(explode('.', $a))));
}
rsort($anos);

// Pega cursos do ano selecionado
$cursos = unserialize(file_get_contents("data/{$ano}.cursos"));

if (isset($curso)) $bixos = unserialize(file_get_contents("data/{$ano}.bixos"));


?>
<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
  <!--<![endif]-->
  <head>
    <meta charset="utf-8" />
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
    <title>Listão UFRGS desde 2008 por Curso</title>
    <meta content="Ferramenta que permite listar de maneira rápida todos os Bixos UFRGS desde 2008/1 até o presente, separados por curso." name="description" />
    <meta content="Leonel Rocha" name="author" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <?php if (stristr($_SERVER["HTTP_USER_AGENT"],'facebook') !== false) { ?>
        <meta property="og:title" content="Listão UFRGS desde 2008 por Curso" />
        <meta property="og:url" content="http://ufr.gs/listao" />
        <meta property="og:type" content="website" />
        <meta property="fb:admins" content="662949469" />
        <meta property="og:image" content="http://ufr.gs/favicon.png" />
    <?php } ?>
    <link href="/stylesheets/style.css" rel="stylesheet" />
    <script src="/js/modernizr-2.0.6.min.js"></script>
  </head>
  <body>

    <div id="main">
      <header>
        <h1>Listão da UFRGS por Curso</h1>
        <div id="busca">
          <form action="/listao" method="get">
            <select name="ano" id="ano">
            <?php
            foreach ($anos as $a):
              if ($ano == $a) $selected = " selected";
              else $selected = '';
            ?>
              <option value="<?php echo $a ?>"<?php echo  $selected?>><?php echo $a ?></option>
            <?php endforeach; ?>
            </select>
            <select name="curso" id="cursos">
              <option value="todos"<?php if ($curso == 'todos' || !isset($curso)) echo " selected" ?>>Todos</option>
            <?php
            foreach ($cursos as $i => $c):
              if (isset($curso) && $curso != 'todos' && $curso == $i) {
                $selected = " selected";
              }
              else $selected = '';
            ?>
              <option value="<?php echo $i ?>"<?php echo $selected?>><?php echo $c ?></option>
            <?php endforeach; ?>
            </select>
            <select name="semestre">
              <option value="0"<?php if ($semestre == 0) echo " selected" ?>>Todos</option>
              <option value="1"<?php if ($semestre == 1) echo " selected" ?>>1º Semestre</option>
              <option value="2"<?php if ($semestre == 2) echo " selected" ?>>2º Semestre</option>
            </select>
            <input type="submit" value="Ver">
          </form>
        </div><!--/busca -->
      </header>

      <?php if (isset($bixos)): ?>
        <div id="bixos">
          <h2>Bixos <?php echo $cursos[$curso] ?> UFRGS <?php echo $ano; ?><? if($semestre != 0) echo "/{$semestre}"; ?></h2>
          <ul>
            <?php
            foreach ($bixos as $b):
              if (($curso    == 'todos' || $b['curso']    == $curso)
               && ($semestre == 0       || $b['semestre'] == $semestre)):
            ?>
              <li>
                <?php if (isset($_GET['fb'])): ?>
                  <a href="http://www.facebook.com/search/results.php?q=<?php echo  htmlentities($b['nome'], ENT_QUOTES,'UTF-8'); ?>" target="_blank">[fb]</a>
                <?php endif; ?>
                <?php echo  $b['nome']; ?>
              </li>
            <?php
              endif;
            endforeach;
            ?>
          </ul>
        </div><!--/bixos -->
      <?php else: ?>
        <div class="aviso">
          <h2>Divulgação do Listão 2013 da UFRGS</h2>
          Está disponível o Listão 2013 POR CURSOS, utilize a busca para encontrar sua barra!
        </div>
      <?php endif; // isset($bixos); ?>

      <footer>
        <div id="social">
          <div class="fb-like" data-href="http://ufr.gs/" data-send="false" data-layout="button_count" data-width="95" data-show-faces="false"></div>
          <div class="g-plusone" data-size="medium" data-href="http://ufr.gs/listao"></div>
        </div><!--/social -->
        <strong>ufr.gs/listao</strong> - Desenvolvido por Leonel Rocha (RP UFRGS 2010/1 | CIC UFRGS 2012/1)<br />
        Esse aplicativo não possui nenhum vínculo com a UFRGS.<br /> Todos os dados estão disponíveis em <a href="http://ufrgs.br/" target="_blank">ufrgs.br</a>
        Não há nenhum tipo de garantia sobre os dados aqui contidos.
      </footer>
    </div><!--/main -->
    <div id="fb-root"></div>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="/js/plugins.js"></script>
    <script src="/js/script.js"></script>
    <script>
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-2277461-9']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>
    <!--[if lt IE 7]>
    <script src="//ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
    <script>
      window.attachEvent("onload",function(){CFInstall.check({mode:"overlay"})});
    </script>
    <![endif]-->
  </body>
</html>
