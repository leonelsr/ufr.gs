/* Author: Leonel Rocha (leonel dot rocha at ufrgs dot br)

*/

    $(document).ready(function(e) {
        $('#ano').change(function(e) {
	      $("#cursos").html('<option>Carregando...</option>');
	      $.getJSON('/cursos.json', {ano: $('#ano').val()}, function (data) {
	        $("#cursos").empty();
	        $('<option></option>').val('todos').html('Todos').appendTo('#cursos');
	        $.each(data, function (i, v) {
	          $('<option></option>').val(i).html(v).appendTo('#cursos');
	        });
	      });
        });
    });




















