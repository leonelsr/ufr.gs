<?php
class Lsdk {
	public $namespace;
	public $db;
	private $settings = array(
		'db' => array(
		  'rockup' => array(
			'dbHost' => '',				  
			'dbName' => '',
			'dbUser' => '',
			'dbPass' => ''
		  ),
		  'collegerock' => array (
			'dbHost' => '',				  
			'dbName' => '',
			'dbUser' => '',
			'dbPass' => '',
		  )
		)
	);
	public $arg;
	public function connectDB($arg) {
		try {
		  $dsn = "mysql:host={$this->settings['db'][$arg]['dbHost']};dbname={$this->settings['db'][$arg]['dbName']}";
		  $dbh = new PDO($dsn,
					    $this->settings['db'][$arg]['dbUser'],
					    $this->settings['db'][$arg]['dbPass'],
					    array(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
		  $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		  $dbh->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		  $dbh->exec("SET NAMES utf8");
		  $dbh->exec("SET character_set_connection=utf8");
		  $dbh->exec("SET character_set_client=utf8");
		} catch(PDOException $e){
			echo "Servidor de banco de dados sobrecarregado, a equipe de suporte do nosso Hosting já foi notificada. Tente novemente em alguns instantes! Desculpem o transtorno =)";
			/*
            echo 'Error connecting to MySQL!: '.$e->getMessage();
			echo "\nwith dsn: ".$dsn;
			echo "\nwith setting: ".$setting;
			var_dump($this);
			*/
		}
		return $dbh;
	}
	function Lsdk ($arg) {
		date_default_timezone_set('America/Sao_Paulo');
		mb_internal_encoding('UTF-8');
		mb_http_output('UTF-8');
		mb_http_input('UTF-8');
		mb_language('uni');
		mb_regex_encoding('UTF-8');
		ob_start('mb_output_handler');
		ob_implicit_flush();
		$this->namespace = $arg;
		$this->db = $this->connectDB($arg);
		return $this;
	}
	static function random($min, $max) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "http://www.random.org/sequences/?min={$min}&max={$max}&col=1&format=plain&rnd=new");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$random = explode("\n", curl_exec($ch));
		curl_close($ch);
		
		/*
		$numbers = range($min, $max);
		shuffle($numbers);
		*/

		
		return $random;
	}
	static function randomstr($length, $noshuffle = false) {
		$charray = array_merge(
		  range("a", "z"),
		  range("A", "Z"),
		  range("0", "9")
		);
		if (!$noshuffle) shuffle($charray);
		$chrs = implode("", $charray);
	  
		for($i = 0; $i < $length; $i++) {
		   $random .= substr($chrs,(mt_rand()%(strlen($chrs))), 1);
		}
		return $random;
	}
	static function txt () {
		header("Content-type: text/plain; charset=utf-8");
	}
	static function html () {
		header("Content-type: text/html; charset=utf-8");
	}
	static function log ($texto) {
		$texto = "[" . date('Y-m-d H:i:s') . "] " . iconv('UTF-8', 'ISO-8859-1', $texto) . "\n";
		file_put_contents('/home/rockup/php_erro', $texto, FILE_APPEND);
	}
}
?>