<?php

//http://listao.ufrgs.br/listao/letra_L.html

ini_set('display_errors', '1');
error_reporting(E_ALL);

mb_internal_encoding("UTF-8");
//require('lsdk.php');
require('simple_html_dom.php');
//lsdk::txt();



function curl_get($url, array $get = array(), array $options = array()) 
{    
    $defaults = array( 
        CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get), 
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_TIMEOUT => 30
    ); 
    
    $ch = curl_init(); 
    curl_setopt_array($ch, ($options + $defaults)); 
    if( ! $result = curl_exec($ch)) 
    { 
        trigger_error(curl_error($ch)); 
    } 
    curl_close($ch); 
    return $result; 
}

$bixos = array();

foreach (range("A", "Z") as $l) {
    $letra = iconv('ISO-8859-1', 'UTF-8', curl_get("http://listao.ufrgs.br/listao/letra_{$l}.html"));
    $html = str_get_html($letra);

    foreach($html->find('.linha') as $element) {
        $e             =  str_get_html($element);
        $b             = array();
        $b['id']       = (string) $e->find('.span1', 0);
        $b['nome']     = (string) $e->find('.span2', 0);
        $b['semestre'] = (string) $e->find('.span3', 0);
        $b['curso']    = (string) $e->find('.span4', 0);
        $bixos[] = $b;
    }
}

file_put_contents('data/2013.data', serialize($bixos));
echo('feito');

/*

foreach ($listao as $n) {
    $n = trim($n);
    $p = array();
    
    $p['id'] = mb_substr($n, 0, 8);
    $p['nome'] = trim(mb_substr($n, 11, 51));
    $p['semestre'] = mb_substr($n, 62, 1);
    $p['curso'] = mb_substr($n, 66);
    
    $bixos[] = $p;
    
    if (!in_array($p['curso'], $cursos)) $cursos[] = $p['curso'];
}
*/