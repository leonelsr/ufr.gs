<?php


mb_internal_encoding("UTF-8");

$data = unserialize(file_get_contents("bixos.data"));

$cursos = array();

foreach ($data[$_GET['ano']] as $n) {
	if (!in_array($n['curso'], $cursos)) $cursos[] = $n['curso'];
}
sort($cursos);


print(json_encode($cursos));