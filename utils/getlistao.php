<?php


mb_internal_encoding("UTF-8");
require('lsdk.php');
lsdk::txt();


function curl_get($url, array $get = array(), array $options = array()) 
{    
    $defaults = array( 
        CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get), 
        CURLOPT_HEADER => 0, 
        CURLOPT_RETURNTRANSFER => TRUE, 
        CURLOPT_TIMEOUT => 30 
    ); 
    
    $ch = curl_init(); 
    curl_setopt_array($ch, ($options + $defaults)); 
    if( ! $result = curl_exec($ch)) 
    { 
        trigger_error(curl_error($ch)); 
    } 
    curl_close($ch); 
    return $result; 
}

$ano = $_GET['ano'];

$listao = '';
foreach (range("A", "Z") as $l) {
	$letra = iconv('ISO-8859-1', 'UTF-8', curl_get("http://www.ufrgs.br/coperse/cv{$ano}/listao/CV{$ano}_{$l}.htm"));
	if ($ano == 2010) $letra = explode("\n", $letra);
	else  $letra = explode("\r\n", $letra);
	array_pop($letra);
	array_shift($letra);
	
	
	$letra = strip_tags(implode("\n", $letra));
	$letra = str_replace("          * - há outro candidato com Nome idêntico. Confirme pelo número de inscrição.\n", '', $letra);
	$letra = str_replace("*", ' ', $letra);
	$letra = str_replace("Não há candidatos classificados com esta inicial de nome.\n", '', $letra);
	$listao .= $letra;
}
$listao = explode("\n", trim($listao));

$bixos = array();
$cursos = array();

foreach ($listao as $n) {
	$n = trim($n);
	$p = array();
	
	$p['id'] = mb_substr($n, 0, 8);
	$p['nome'] = trim(mb_substr($n, 11, 51));
	$p['semestre'] = mb_substr($n, 62, 1);
	$p['curso'] = mb_substr($n, 66);
	
	$bixos[] = $p;
	
	if (!in_array($p['curso'], $cursos)) $cursos[] = $p['curso'];
}

sort($cursos);

$data = unserialize(file_get_contents("bixos.data"));
$data[$ano] = $bixos;
file_put_contents("bixos.data", serialize($data));















